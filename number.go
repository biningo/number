package number

/**
*@Author icepan
*@Date 8/3/21 15:23
*@Describe
**/

func Sum(nums ...int) int {
	s := 0
	for _, v := range nums {
		s += v
	}
	return s
}
